$(function(){
    $(".play-button").click(function(){
        $(".videoModal").removeClass("d-none");
        setTimeout(() => {
            $(".videoModal").css("opacity","1");
        }, 300);
    });

    $(".videoModal .close-modal").click(function(){
        $(".videoModal").css("opacity","0");
        setTimeout(() => {
            $(".videoModal").addClass("d-none");
        }, 300);
    });
});